
create table log (
    id bigserial primary key,
    data varchar(1024)
);

CREATE TABLE program
(
    id bigint NOT NULL,
    image character varying(255),
    name character varying(255),
    program_id integer NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE action_detail
(
    id bigint NOT NULL,
    actions_detail_id integer NOT NULL,
    hold_size integer NOT NULL,
    name character varying(255),
    type character varying(255),
    program_id bigint,
    PRIMARY KEY (id)
);

ALTER TABLE action_detail ADD CONSTRAINT fk_prg_act FOREIGN KEY (program_id) REFERENCES program (id);

CREATE TABLE program_categories
(
    program_id bigint NOT NULL,
    id integer NOT NULL,
    language character varying(255),
    name character varying(255)
);

ALTER TABLE program_categories ADD CONSTRAINT fk_prg_cat FOREIGN KEY (program_id) REFERENCES program (id);

CREATE TABLE tariff
(
    id bigint NOT NULL,
    action_id integer NOT NULL,
    name character varying(255) COLLATE pg_catalog."default",
    tariff_id integer NOT NULL,
    action_detail_id bigint,
    PRIMARY KEY (id)
);

ALTER TABLE tariff ADD CONSTRAINT fk_tar_act FOREIGN KEY (action_detail_id) REFERENCES action_detail (id);

CREATE SEQUENCE hibernate_sequence
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

