package ru.kalugin.connetors.repo;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.kalugin.connetors.dto.Program;

@Repository
public interface ProgramsDtoRepo extends PagingAndSortingRepository<Program, Long> {
    Program findByProgramId(int id);
}
