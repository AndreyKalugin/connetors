package ru.kalugin.connetors.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.kalugin.connetors.dto.Program;
import ru.kalugin.connetors.repo.ProgramsDtoRepo;

@RestController
@RequestMapping("admitad")
public class Controller {

    @Autowired
    private ProgramsDtoRepo programsDtoRepo;

    @GetMapping(path = "/programs/id")
    public Program getProgramsById(@RequestParam int id) {
        return programsDtoRepo.findByProgramId(id);
    }

    @GetMapping(path = "/programs/with-pagination")
    public Page<Program> getProgramsWithPagination(@RequestParam int page, @RequestParam int size) {
        Pageable pageable = PageRequest.of(0, 2);
        return programsDtoRepo.findAll(pageable);
    }

    @GetMapping(path = "/programs/all")
    public Iterable<Program> getAllPrograms() {
        return programsDtoRepo.findAll();
    }
}
