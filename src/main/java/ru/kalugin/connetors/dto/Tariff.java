package ru.kalugin.connetors.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@EqualsAndHashCode
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Tariff {
    @Id
    @JsonIgnore
    @GeneratedValue
    private long id;
    @JsonProperty("action_id")
    private int actionId;
    private String name;
    @JsonProperty("id")
    private int tariffId;
    @JsonBackReference
    @ManyToOne
    private ActionDetail actionDetail;
}
