package ru.kalugin.connetors.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@EqualsAndHashCode
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActionDetail {
    @Id
    @JsonIgnore
    @GeneratedValue
    private long id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "actionDetail")
    @JsonManagedReference
    private List<Tariff> tariffs;
    private String type;
    private String name;
    @JsonProperty("id")
    private int actionsDetailId;
    @JsonProperty("hold_size")
    private int holdSize;
    @JsonBackReference
    @ManyToOne
    private Program program;
}
