package ru.kalugin.connetors.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@EqualsAndHashCode
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Program {
     @Id
     @JsonIgnore
     @GeneratedValue
     private long id;
     @JsonProperty("id")
     private int programId;
     private String name;
     @JsonProperty("actions_detail")
     @OneToMany(cascade = CascadeType.ALL, mappedBy = "program")
     @JsonManagedReference
     private List<ActionDetail> actionsDetails;
     private String image;
     @ElementCollection(fetch = FetchType.EAGER)
     private List<Category> categories;
}
