package ru.kalugin.connetors.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import ru.kalugin.connetors.dto.TokenDto;

import java.util.Collections;
import java.util.Optional;

import static java.util.Optional.ofNullable;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

@Service
public class SecurityService {

    private String accessToken;
    private String refreshToken;
    private RestTemplate restTemplate;
    static final private String EMPTY_TOKEN = "";
    @Value("${token.url}")
    private String url;
    @Value("${token.basic.auth}")
    private String basicAuth;

    public SecurityService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getAccessToken() {
        return ofNullable(accessToken).orElse(EMPTY_TOKEN);
    }

    public String getRefreshToken() {
        return Optional.of(refreshToken).orElse(EMPTY_TOKEN);
    }

    @EventListener({ApplicationReadyEvent.class})
    public void getValidationsTokens(){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth(basicAuth);
        headers.add(CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);

        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("grant_type", "client_credentials");
        requestBody.add("client_id", "Mt44BBCMKwYD5BEPLiY9FFdaul21xn");
        requestBody.add("scope", "advcampaigns websites public_data advcampaigns_for_website");

        HttpEntity<?> entity = new HttpEntity<>(requestBody, headers);

        var dto = restTemplate.exchange(url, HttpMethod.POST, entity, new ParameterizedTypeReference<TokenDto>() {})
                                         .getBody();

        accessToken = dto.getAccessToken();
        refreshToken = dto.getRefreshToken();
    }


}
