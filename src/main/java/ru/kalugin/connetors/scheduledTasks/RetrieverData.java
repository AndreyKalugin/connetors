package ru.kalugin.connetors.scheduledTasks;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.kalugin.connetors.dto.Results;
import ru.kalugin.connetors.repo.ProgramsDtoRepo;
import ru.kalugin.connetors.services.SecurityService;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class RetrieverData {

    private RestTemplate restTemplate;
    private SecurityService securityService;
    private ProgramsDtoRepo programsDtoRepo;
    @Value("${access.token.url}")
    private String url;

    public RetrieverData(RestTemplate restTemplate, SecurityService securityService, ProgramsDtoRepo programsDtoRepo) {
        this.restTemplate = restTemplate;
        this.securityService = securityService;
        this.programsDtoRepo = programsDtoRepo;
    }

    @Scheduled(fixedDelayString = "${fixedRate.in.milliseconds:default}", initialDelay = 10000)
    void getDataFromAdmitad() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBearerAuth(securityService.getAccessToken());
        HttpEntity<?> entity = new HttpEntity<>(headers);

        String urlTemplate = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("limit", "{limit}")
                .encode()
                .toUriString();

        Map<String, Integer> params = new HashMap<>();
        params.put("limit", 10);

        var dto = restTemplate.exchange(urlTemplate, HttpMethod.GET, entity, Results.class, params)
                .getBody().getResults();

        dto.forEach(programDto -> {
            if(Objects.isNull(programDto)) return;
            var program = programsDtoRepo.findByProgramId(programDto.getProgramId());
            if (Objects.nonNull(program) && !program.equals(programDto)) {
                programsDtoRepo.delete(program);
                programsDtoRepo.save(programDto);
            }
            if(Objects.isNull(program)){
                programsDtoRepo.save(programDto);
            }
        });
    }
}
